package br.com.mobile.mobiledevelopers

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class PaisAdapter (
    val paises: List<Pais>,
    val onClick: (Pais) -> Unit):
    RecyclerView.Adapter<PaisAdapter.PaisesViewHolder>() {
    // ViewHolder com os elementos da tela
    class PaisesViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val cardNome: TextView
        val cardImg : ImageView
        var cardProgress: ProgressBar
        var cardView: CardView
        init {
            cardNome = view.findViewById<TextView>(R.id.cardNome)
            cardImg = view.findViewById<ImageView>(R.id.cardImg)
            cardProgress = view.findViewById<ProgressBar>(R.id.cardProgress)
            cardView = view.findViewById<CardView>(R.id.cardPaises)
        }
    }
    // Quantidade de disciplinas na lista
    override fun getItemCount() = this.paises.size
    // inflar layout do adapter
    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int): PaisesViewHolder {
// infla view no adapter
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.adapter_pais, parent, false)
// retornar ViewHolder
        val holder = PaisesViewHolder(view)
        return holder
    }
    override fun onBindViewHolder(holder: PaisesViewHolder, position: Int) {
        val context = holder.itemView.context
// recuperar objeto disciplina
        val pais = paises[position]
// atualizar dados de disciplina
        holder.cardNome.text = pais.nome
        holder.cardProgress.visibility = View.VISIBLE
// download da imagem
        Picasso.with(context).load(pais.bandeira).fit().into(holder.cardImg,

            object: com.squareup.picasso.Callback{
                override fun onSuccess() {
                    holder.cardProgress.visibility = View.GONE
                }
                override fun onError() {
                    holder.cardProgress.visibility = View.GONE
                }
            })

// adiciona evento de clique
        holder.itemView.setOnClickListener {onClick(pais)}
    }
}