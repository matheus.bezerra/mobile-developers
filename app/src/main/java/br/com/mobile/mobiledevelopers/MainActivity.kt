package br.com.mobile.mobiledevelopers

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import br.com.mobile.mobiledevelopers.databinding.LoginBinding
import kotlinx.android.synthetic.main.login.view.*


class MainActivity : AppCompatActivity() {

    private val binding by lazy{
        LoginBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)



        fun onClickLogin(){

            val intent = Intent(this, TelaInicialActivity::class.java)
            startActivity(intent)

    }
        binding.loginButton.setOnClickListener {onClickLogin() }
    }
    
}