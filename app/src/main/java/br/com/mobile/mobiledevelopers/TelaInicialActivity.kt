package br.com.mobile.mobiledevelopers

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.mobile.mobiledevelopers.databinding.ActivityTelaInicialBinding
import com.google.android.material.navigation.NavigationView

class TelaInicialActivity : DebugActivity(), NavigationView.OnNavigationItemSelectedListener {

    private val context: Context get() = this
    private var paises = listOf<Pais>()

    private val binding by lazy {
        ActivityTelaInicialBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbarInclude.toolbar)
        binding.recyclerPaises?.layoutManager = LinearLayoutManager(context)
        binding.recyclerPaises?.itemAnimator = DefaultItemAnimator()
        binding.recyclerPaises?.setHasFixedSize(true)
        supportActionBar?.title = ""
        configuraMenuLateral()
        binding.botaoSair.setOnClickListener {cliqueSair()}
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
    // infla o menu com os botões da ActionBar
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
// id do item clicado
        val id = item?.itemId
        if (id == R.id.action_buscar) {
            Toast.makeText(context, "Botão de buscar",
                Toast.LENGTH_LONG).show()

        } else if (id == R.id.action_atualizar) {
            Toast.makeText(context, "Botão de atualizar",
                Toast.LENGTH_LONG).show()

        }
        return super.onOptionsItemSelected(item)
    }

    private fun configuraMenuLateral() {
        var toogle = ActionBarDrawerToggle(
            this,
            binding.layoutMenuLateral,
            binding.toolbarInclude.toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close)
        binding.layoutMenuLateral.addDrawerListener(toogle)
        toogle.syncState()
        binding.menuLateral.setNavigationItemSelectedListener(this)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_diciplinas -> {
                Toast.makeText(this, "Clicou Disciplinas", Toast.LENGTH_SHORT).show()
            }
            R.id.nav_mensagens -> {
                Toast.makeText(this, "Clicou Mensagens", Toast.LENGTH_SHORT).show()
            }
            R.id.nav_forum -> {
                Toast.makeText(this, "Clicou Forum", Toast.LENGTH_SHORT).show()
            }
            R.id.nav_localizacao -> {
                Toast.makeText(this, "Clicou Localização", Toast.LENGTH_SHORT).show()
            }
            R.id.nav_config -> {
                Toast.makeText(this, "Clicou Config", Toast.LENGTH_SHORT).show()
            }
        }
        binding.layoutMenuLateral.closeDrawer(GravityCompat.START)
        return true
    }

    fun cliqueSair() {
        val returnIntent = Intent();
        returnIntent.putExtra("result","Saída do BrewerApp");
        setResult(Activity.RESULT_OK,returnIntent);
        finish();
    }

    override fun onResume() {
        super.onResume()
        taskPaises()
    }

    fun taskPaises() {
        paises = PaisService.getPaises(context)

        binding.recyclerPaises?.adapter = PaisAdapter(paises) {onClickPais(it)}
    }

    fun onClickPais(pais: Pais) {
        Toast.makeText(context, "Clicou país ${pais.nome}", Toast.LENGTH_SHORT)
            .show()
        val intent = Intent(context, PaisActivity::class.java)
        intent.putExtra("país", pais)
        startActivity(intent)
    }
}