package br.com.mobile.mobiledevelopers

import java.io.Serializable

class Pais : Serializable {
    var id: Long = 0
    var nome = ""
    var capital = ""
    var bandeira = ""
    var continente = ""
    var populacao = ""
    var latitude = ""
    var longitude = ""

    override fun toString(): String {
        return "País(nome='$nome')"
    }
}