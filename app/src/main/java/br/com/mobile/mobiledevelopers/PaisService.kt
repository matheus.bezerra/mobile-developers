package br.com.mobile.mobiledevelopers

import android.content.Context

object PaisService {

    fun getPaises (context: Context): List<Pais> {
        val paises = mutableListOf<Pais>()

        for (i in 1..10) {
            val p = Pais()
            p.nome = "Pais $i"
            p.capital = "Ementa Disciplina $i"
            p.bandeira = "https://www.meusdicionarios.com.br/wp-content/uploads/2016/05/sh_bandeira-da-alemanha_386181889.jpg"
            p.continente = "Continente $i"
            p.populacao = "População $i"
            p.latitude = "Latitude $i"
            p.longitude = "Longitude $i"
            paises.add(p)
        }
        return paises
    }
}