package br.com.mobile.mobiledevelopers

import android.os.Bundle
import br.com.mobile.mobiledevelopers.databinding.ActivityPaisBinding
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.toolbar.*

class PaisActivity : DebugActivity() {
    private val binding by lazy {
        ActivityPaisBinding.inflate(layoutInflater)
    }
    var pais: Pais? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        pais = intent.getSerializableExtra("pais") as Pais


        setSupportActionBar(toolbar)

        supportActionBar?.title = pais?.nome

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        binding.nomePais.text = pais?.nome
        Picasso.with(this).load(pais?.bandeira).fit().into(

            binding.imagemPais,
            object: com.squareup.picasso.Callback{
                override fun onSuccess() {}
                override fun onError() { }
            })

    }
}